package org.hackit.descubrerd.activities;

import java.io.IOException;
import java.net.MalformedURLException;

import org.hackit.descubrerd.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		TextView textView =(TextView)findViewById(R.id.nameTextView);
		TextView textView1 =(TextView)findViewById(R.id.pointsTextView);
		final ImageView imageView=(ImageView)findViewById(R.id.profileImageView);
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		textView.setText(prefs.getString("user_name", ""));
		textView1.setText(String.valueOf(prefs.getInt("user_points", 500))+" puntos");

		
		 final Handler handler = new Handler() {
			  @Override
			  public void handleMessage(Message msg) {
			//display each item in a single line
					imageView.setImageBitmap((Bitmap)msg.obj);
			     }
			 };
			 
		Thread background=new Thread(new Runnable() {

			   @Override
			   public void run() {
					try {
						Bitmap bmp=null;
						bmp = BitmapFactory.decodeStream(new java.net.URL("http://graph.facebook.com/"+prefs.getString("user_id", "")+"/picture?type=large").openStream());
						;
						Message msg=new Message();
						msg.obj=bmp;
						handler.sendMessage(msg);
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			
			    
			   }
			  });

			  background.start();
			  
		
		
	}
	
}
