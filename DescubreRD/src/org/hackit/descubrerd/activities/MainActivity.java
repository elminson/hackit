package org.hackit.descubrerd.activities;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.hackit.descubrerd.MisDescubrimientosActivity;
import org.hackit.descubrerd.R;
import org.hackit.descubrerd.entities.Place;
import org.hackit.descubrerd.utils.NavDrawerItem;
import org.hackit.descubrerd.utils.NavDrawerListAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;

import com.arellomobile.android.push.BasePushMessageReceiver;
import com.arellomobile.android.push.PushManager;
import com.arellomobile.android.push.utils.RegisterBroadcastReceiver;
import com.facebook.Session;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tackmobile.SliderPuzzleActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemSelectedListener,OnMarkerClickListener,OnItemClickListener  {
	 private String[] navMenuTitles;
	    private DrawerLayout mDrawerLayout;
	    private ListView mDrawerList;
	    private ActionBarDrawerToggle mDrawerToggle;
	    private CharSequence mDrawerTitle;
	    private CharSequence mTitle;
	    private ArrayList<NavDrawerItem> navDrawerItems;
	    private NavDrawerListAdapter adapter;
	    private Place[] places;
	//    private JSONArray
	private GoogleMap mMap;
	private  LatLngBounds DR = new LatLngBounds(
			  new LatLng(19, -70.666667), new LatLng(19, -70.666667));
	  private static final ScheduledExecutorService worker = 
			  Executors.newSingleThreadScheduledExecutor();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		

	        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
	        mDrawerList = (ListView) findViewById(R.id.left_drawer);

	        navDrawerItems = new ArrayList<NavDrawerItem>();
	        
	        // adding nav drawer items to array
	        // load slide menu items
	        navMenuTitles = getResources().getStringArray(R.array.menu);
	 
	        // Home
	        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], R.drawable.icon_profile));
	        // Find People
	        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], R.drawable.icon_top10));
	        // Photos
	        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], R.drawable.icon_descubre));
	        // Communities, Will add a counter here
	        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3],  R.drawable.icon_info));
	         
	 
	       
	 
	        // setting the nav drawer list adapter
	        adapter = new NavDrawerListAdapter(getApplicationContext(),
	                navDrawerItems);
	        mDrawerList.setAdapter(adapter);
	        // Set the list's click listener
	        mDrawerList.setOnItemClickListener(this);
	        
	        
		 FragmentManager myFragmentManager = getFragmentManager();
		  MapFragment myMapFragment 
		   = (MapFragment)myFragmentManager.findFragmentById(R.id.map);
		  mMap = myMapFragment.getMap();
		  
		  Spinner spinner= (Spinner)this.findViewById(R.id.places);
	    spinner.setOnItemSelectedListener(this);
		  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DR.getCenter(),6.7f));
		  
		  mMap.getUiSettings().setZoomGesturesEnabled(false);
		 mMap.getUiSettings().setZoomControlsEnabled(false);
		  
		 mMap.getUiSettings().setScrollGesturesEnabled(false);
		  //Register receivers for push notifications
		 
		 mMap.setOnMarkerClickListener(this);
	      registerReceivers();
	 
	      //Create and start push manager
	      PushManager pushManager = new PushManager(this, "3623B-3B441", "794143075485");
	      pushManager.onStartup(this);
	 
	      checkMessage(getIntent());
		
	      JSONObject obj;
		try {
			obj = loadJSONFromAsset("json_places.json");
		
	      processJSON(obj.getJSONArray("places"));
	      Log.d("T","F");
	      
	      refreshMarkers();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
	        mDrawerToggle = new ActionBarDrawerToggle(
	                this,                  /* host Activity */
	                mDrawerLayout,         /* DrawerLayout object */
	                R.drawable.ic_launcher,  /* nav drawer icon to replace 'Up' caret */
	                R.string.drawer_open,  /* "open drawer" description */
	                R.string.drawer_close  /* "close drawer" description */
	                ) {

	            /** Called when a drawer has settled in a completely closed state. */
	            public void onDrawerClosed(View view) {
	                super.onDrawerClosed(view);
	                getActionBar().setTitle(mTitle);
	            }

	            /** Called when a drawer has settled in a completely open state. */
	            public void onDrawerOpened(View drawerView) {
	                super.onDrawerOpened(drawerView);
	                getActionBar().setTitle(mDrawerTitle);
	            }
	        };

	        // Set the drawer toggle as the DrawerListener
	        mDrawerLayout.setDrawerListener(mDrawerToggle);

	        getActionBar().setDisplayHomeAsUpEnabled(true);
	        getActionBar().setDisplayShowTitleEnabled(false);
	        getActionBar().setHomeButtonEnabled(true);  
		
	}
	
	private void processJSON(JSONArray array) throws JSONException{
		      places=new Place[array.length()];
		     for(int i=0;i<array.length();i++){
		    	 JSONObject jObj=array.getJSONObject(i);
		    	Place p=new Place();
		    	p.setActive(jObj.getBoolean("isActive"));
		    	p.setLatitude(jObj.getDouble("latitude"));
		    	p.setLongitude(jObj.getDouble("longitude"));
		    	p.setCity(jObj.getString("city"));
		    	p.setDescription(jObj.getString("description"));
		    	p.setName(jObj.getString("name"));
		    	p.setId(jObj.getInt("id"));
		    	p.setImageUrl(jObj.getString("picture"));
		    	places[i]=p;
		    	
		     }
		  
	}
	
	
	public void refreshMarkers(){
		if(places!=null){
			mMap.clear();
			for(int i=0;i<places.length;i++){
				
				 mMap.addMarker(new MarkerOptions()
		 	        .position(new LatLng (places[i].getLatitude(),places[i].getLongitude()))
		 	        .icon(places[i].isActive()?BitmapDescriptorFactory.fromResource(R.drawable.conquistado):BitmapDescriptorFactory.fromResource(R.drawable.question_mark))
		 	        .title(places[i].isActive()?places[i].getName():String.valueOf(places[i].getId())));
				
			}
		}
	
	}
	  @Override
	    protected void onPostCreate(Bundle savedInstanceState) {
	        super.onPostCreate(savedInstanceState);
	        // Sync the toggle state after onRestoreInstanceState has occurred.
	        mDrawerToggle.syncState();
	    }

	    @Override
	    public void onConfigurationChanged(Configuration newConfig) {
	        super.onConfigurationChanged(newConfig);
	        mDrawerToggle.onConfigurationChanged(newConfig);
	    }

	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        // Pass the event to ActionBarDrawerToggle, if it returns
	        // true, then it has handled the app icon touch event
	        if (mDrawerToggle.onOptionsItemSelected(item)) {
	          return true;
	        }
	        // Handle your other action bar items...

	        return super.onOptionsItemSelected(item);
	    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public void onResume()
	{
	    super.onResume();
	    refreshMarkers();
	    //Re-register receivers on resume
	    registerReceivers();
	   
	}
	 
	@Override
	public void onPause()
	{
	    super.onPause();
	 
	    //Unregister receivers on pause
	    unregisterReceivers();
	}

	

		//Registration receiver
		 BroadcastReceiver mBroadcastReceiver = new RegisterBroadcastReceiver()
		 {
		     @Override
		     public void onRegisterActionReceive(Context context, Intent intent)
		     {
		         checkMessage(intent);
		     }
		 };
		  
		 //Push message receiver
		 private BroadcastReceiver mReceiver = new BasePushMessageReceiver()
		 {
		     @Override
		     protected void onMessageReceive(Intent intent)
		     {
		         //JSON_DATA_KEY contains JSON payload of push notification.
		         try {
					showMessage(new JSONObject(intent.getExtras().getString(JSON_DATA_KEY)).getString("title"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		     }
		 };
		  
		 //Registration of the receivers
		 public void registerReceivers()
		 {
		     IntentFilter intentFilter = new IntentFilter(getPackageName() + ".action.PUSH_MESSAGE_RECEIVE");
		  
		     registerReceiver(mReceiver, intentFilter);
		      
		     registerReceiver(mBroadcastReceiver, new IntentFilter(getPackageName() + "." + PushManager.REGISTER_BROAD_CAST_ACTION));       
		 }
		  
		 public void unregisterReceivers()
		 {
		     //Unregister receivers on pause
		     try
		     {
		         unregisterReceiver(mReceiver);
		     }
		     catch (Exception e)
		     {
		         // pass.
		     }
		      
		     try
		     {
		         unregisterReceiver(mBroadcastReceiver);
		     }
		     catch (Exception e)
		     {
		         //pass through
		     }
		 }
		 private void checkMessage(Intent intent)
		 {
		     if (null != intent)
		     {
		         if (intent.hasExtra(PushManager.PUSH_RECEIVE_EVENT))
		         {
		             showMessage("push message is " + intent.getExtras().getString(PushManager.PUSH_RECEIVE_EVENT));
		         }
		         else if (intent.hasExtra(PushManager.REGISTER_EVENT))
		         {
		             showMessage("Descubre tu pais");
		         }
		         else if (intent.hasExtra(PushManager.UNREGISTER_EVENT))
		         {
		             showMessage("unregister");
		         }
		         else if (intent.hasExtra(PushManager.REGISTER_ERROR_EVENT))
		         {
		             showMessage("register error");
		         }
		         else if (intent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT))
		         {
		             showMessage("unregister error");
		         }
		  
		         resetIntentValues();
		     }
		 }
		  
		 /**
		  * Will check main Activity intent and if it contains any PushWoosh data, will clear it
		  */
		 private void resetIntentValues()
		 {
		     Intent mainAppIntent = getIntent();
		  
		     if (mainAppIntent.hasExtra(PushManager.PUSH_RECEIVE_EVENT))
		     {
		         mainAppIntent.removeExtra(PushManager.PUSH_RECEIVE_EVENT);
		     }
		     else if (mainAppIntent.hasExtra(PushManager.REGISTER_EVENT))
		     {
		         mainAppIntent.removeExtra(PushManager.REGISTER_EVENT);
		     }
		     else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_EVENT))
		     {
		         mainAppIntent.removeExtra(PushManager.UNREGISTER_EVENT);
		     }
		     else if (mainAppIntent.hasExtra(PushManager.REGISTER_ERROR_EVENT))
		     {
		         mainAppIntent.removeExtra(PushManager.REGISTER_ERROR_EVENT);
		     }
		     else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT))
		     {
		         mainAppIntent.removeExtra(PushManager.UNREGISTER_ERROR_EVENT);
		     }
		  
		     setIntent(mainAppIntent);
		 }
		  
		 private void showMessage(String message)
		 {
		     Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		 }
		 @Override
		 protected void onNewIntent(Intent intent)
		 {
		     super.onNewIntent(intent);
		     setIntent(intent);
		  
		     checkMessage(intent);
		  
		     setIntent(new Intent());
		 }


		
		
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int p,
			long arg3) {
		// TODO Auto-generated method stub
		switch(p){
		   case 1:
			   mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(19.423209, -70.657250),8f));
			    break;
		   case 2:
			   mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(18.666398, -68.859922),9f));
				break;
		   case 3:
			   mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(18.543165, -70.362420),8f));
				break;
		  default:
			   mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(18.556087, -69.927534),9f));
			  break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public JSONObject loadJSONFromAsset(String fileName) throws JSONException {
        String json = null;
        try {

            InputStream is = getAssets().open(fileName);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return new JSONObject(json);

    }
    public Place getPlaceAtMarker(Marker m){
    	
    	for(int i=0;i<places.length;i++){
    		
    		if(m.getTitle().compareTo(String.valueOf(places[i].getId()))==0){
    			return places[i];
    		}
    	}
    	
    	return places[0];
    }
	@Override
	public boolean onMarkerClick(Marker m) {
		// TODO Auto-generated method stub
		
			Place place=getPlaceAtMarker(m);
	         if(place.isActive())
	        	 return false;
			 Intent intent=new Intent(this,place.getId()%2==0?AdinivaImagenActivity.class:SliderPuzzleActivity.class);
		     intent.putExtra("Place", place);
		    place.setActive(true);
		     this.startActivity(intent);
		    
		     return true;
		
		  
		
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int p, long arg3) {
		// TODO Auto-generated method stub
		 Intent intent=null;
		switch(p){
		    case 0:
		   	 intent=new Intent(this,ProfileActivity.class);
             this.startActivity(intent);
			    break;
		    case 1:
		    	 intent=new Intent(this,RankingsActivity.class);
	             this.startActivity(intent);
				break;
		    case 2:
		    	 intent=new Intent(this,MisDescubrimientosActivity.class);
	             this.startActivity(intent);
				break;
		    case 3:
		    	 intent=new Intent(this,AboutActivity.class);
	             this.startActivity(intent);
				break;
		}
	}

}
