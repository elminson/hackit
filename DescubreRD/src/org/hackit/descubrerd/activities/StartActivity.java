package org.hackit.descubrerd.activities;

import java.util.Arrays;

import org.hackit.descubrerd.R;

import com.arellomobile.android.push.BasePushMessageReceiver;
import com.arellomobile.android.push.PushManager;
import com.arellomobile.android.push.utils.RegisterBroadcastReceiver;
import com.facebook.FacebookException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.OnErrorListener;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;


public class StartActivity extends Activity {
	private static String TAG = "StartActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		  
		  LoginButton authButton = (LoginButton) findViewById(R.id.authButton);
		  authButton.setOnErrorListener(new OnErrorListener() {
		   
		   @Override
		   public void onError(FacebookException error) {
		    Log.i(TAG, "Error " + error.getMessage());
		   }
		  });
		  
		  Session session = Session.getActiveSession();
	     
	        if(session!=null&&session.getState().isOpened()&&!session.getState().isClosed()){
	        	 session.closeAndClearTokenInformation();
	        	 //goToMain();
	        }
	        
	  
		  // set permission list, Don't foeget to add email
		  authButton.setReadPermissions(Arrays.asList("basic_info","email"));
		  // session state call back event
		  authButton.setSessionStatusCallback(new Session.StatusCallback() {
		   
   

		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			// TODO Auto-generated method stub
	
			 if (session.isOpened()) {
	              Log.i(TAG,"Access Token"+ session.getAccessToken());
	              
	              Request.newMeRequest(session,
	                      new Request.GraphUserCallback() {
							@Override
							public void onCompleted(final GraphUser user,
									final Response response) {
								// TODO Auto-generated method stub
						
								if (user == null) 
									return;
								
								final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(StartActivity.this);
								final Editor editor = prefs.edit();
								Log.d("uid",user.getId());
								editor.putString("user_id", user.getId());
								editor.putString("user_name", user.getFirstName() + " "+user.getLastName());
								editor.commit();
							}
	                      }).executeAsync();
	          
	              goToMain();
	              
	            
	          }
		}
		  });
		  
		
	 
	      // other code
 }
	
	
		 @Override
		 public void onActivityResult(int requestCode, int resultCode, Intent data) {
		     super.onActivityResult(requestCode, resultCode, data);
		     Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		 }
		 private void goToMain(){
			 Intent intent=new Intent(StartActivity.this,MainActivity.class);
             StartActivity.this.startActivity(intent);
		 }
		
}
