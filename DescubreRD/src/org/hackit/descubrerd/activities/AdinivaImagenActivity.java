package org.hackit.descubrerd.activities;

import java.util.Random;


import org.hackit.descubrerd.R;
import org.hackit.descubrerd.entities.Place;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class AdinivaImagenActivity extends Activity {

	private ImageView imagenImg;
	private Button responderBtn;
	private RadioGroup destinosRGBtn;
	private RadioButton opcionDestintoRBtn;
	private GridView gridView;
	private String destino;
	private TextView scorePointsTv;
	
	private int scorePointsInt;
	private Place place;
	private AlertDialog.Builder alertDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_adiniva_imagen);
		final Intent intent = getIntent();
		int randomImgInt =0;
		if (intent.hasExtra("Place")){
			place= (Place)intent.
		        getSerializableExtra("Place");
		    randomImgInt =place.getId();
		}else{
	     randomImgInt = new Random().nextInt(4);
		}
		initUi(randomImgInt);
	}
	
	private void initUi(int id){
		
		imagenImg = (ImageView) findViewById(R.id.imagen);
		
		
//		int imgRes = getResources().getIdentifier("destino_img_"+randomImgInt, "id", this.getPackageName());

		Drawable destinoImg = null;
		
		switch (id) {
		case 0:
//			imagenImg.setImageResource(R.drawable.destino_img_0);
			destinoImg = getResources().getDrawable(R.drawable.destino_img_0);
			destino = getString(R.string.faro_colon);
			break;
		case 1:
//			imagenImg.setImageResource(R.drawable.destino_img_1);
			destinoImg = getResources().getDrawable(R.drawable.destino_img_1);
			destino = getString(R.string.monumento_santiago);
			break;
		case 2:
//			imagenImg.setImageResource(R.drawable.destino_img_2);
			destinoImg = getResources().getDrawable(R.drawable.destino_img_2);
			destino = getString(R.string.puentes_samana);
			break;
		case 3:
//			imagenImg.setImageResource(R.drawable.destino_img_3);
			destinoImg = getResources().getDrawable(R.drawable.destino_img_3);
			destino = getString(R.string.basilica_higuey);
			break;
		case 4:
//			imagenImg.setImageResource(R.drawable.destino_img_4);
			destinoImg = getResources().getDrawable(R.drawable.destino_img_4);
			destino = getString(R.string.arco_triunfo);
			break;

		default:
			break;
		}
		
		Bitmap origBitmap = ((BitmapDrawable)destinoImg).getBitmap();
		
		Bitmap scaled = getScaledImage(origBitmap, 4, 4, 204 );
		
		imagenImg.setImageBitmap(scaled);
				
		responderBtn = (Button) findViewById(R.id.responderBtn);
		destinosRGBtn = (RadioGroup) findViewById(R.id.destinosRBtn);
		
		responderBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				// get selected radio button from radioGroup
				int selectedId = destinosRGBtn.getCheckedRadioButtonId();
	 
				if (selectedId == -1){
					Toast.makeText(getApplicationContext(),
						"Debes seleccionar una respuesta.", Toast.LENGTH_SHORT).show();
				}else{
					// find the radiobutton by returned id
					opcionDestintoRBtn = (RadioButton) findViewById(selectedId);
		 
//					Toast.makeText(getApplicationContext(),
//							opcionDestintoRBtn.getText(), Toast.LENGTH_SHORT).show();
					
					// Si el usuario responde correctamente
					if (opcionDestintoRBtn.getText().toString().equals( destino))
						showDialog(true);
					else
						showDialog(false);
					
				}
			}
		});
		
		// Grid view
		gridView = (GridView) findViewById(R.id.gridView);
		

		String[] numbersArr = new String[16];
		
		for (int i =0; i < 16; i++){
			numbersArr[i] = new String("");
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.grid_item, numbersArr);
				
		
		gridView.setAdapter(adapter);
		
		gridView.setOnItemClickListener(new OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
	        	AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
	        	
	        	anim.setDuration(1000);
	        	v.startAnimation(anim);
	        	
	        	anim.setFillAfter(true);
	        	
	        	scorePointsInt = Integer.parseInt( scorePointsTv.getText().toString()); 
	        	
	        	scorePointsInt -= 50;
	        	
	        	scorePointsTv.setText(""+scorePointsInt);
	        	
//	        	v.setBackgroundColor(0x00FFFFFF);
//	            Toast.makeText(getApplicationContext(), "" + ((TextView) v).getText()+" "+position, Toast.LENGTH_SHORT).show();
	        }
	    });
		
		// Para evitar que el gridview se mueva con el scroll
		gridView.setOnTouchListener(new OnTouchListener(){
		    
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_MOVE){
		            return true;
		        }
		        return false;
			}
		});
		
		scorePointsTv = (TextView) findViewById(R.id.scorePointsTv);
		
	}
	
	private Bitmap getScaledImage(Bitmap original, int rows, int columns, int tileSize){
		
		int fullWidth = tileSize * rows;
		int fullHeight = tileSize * columns;
		Bitmap scaledImage = Bitmap.createScaledBitmap(original, fullWidth, fullHeight, true);
		
		return scaledImage;
	}
	
	private boolean showDialog(boolean gano){

		alertDialog = new AlertDialog.Builder(this);
		// Setting Dialog Title
		alertDialog.setTitle("DescubreRD");
		
		// Setting Dialog Message
		
		String mensaje = gano ? "Usted ha ganado "+scorePointsInt+" puntos." : "Usted ha perdido.";
		
		alertDialog.setMessage(mensaje);
		
		// Setting Icon to Dialog
//		alertDialog.setIcon(R.drawable.tick);
		
		alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				 Intent intent=new Intent( AdinivaImagenActivity.this,SummaryActivity.class);
				 intent.putExtra("title", place.getName());
				 intent.putExtra("description", place.getDescription());
				 intent.putExtra("url", place.getImageUrl());
				 intent.putExtra("lat", String.valueOf(place.getLatitude()));
				 intent.putExtra("lon", String.valueOf(place.getLongitude()));
				 place.setActive(true);
				 
				 switch(place.getId()){
				    case 0:
				    	 intent.putExtra("resourceId", R.drawable.destino_img_0);
					     break;
				    case 1:
				    	 intent.putExtra("resourceId", R.drawable.destino_img_1);
						 break;
				    case 2:
				    	 intent.putExtra("resourceId", R.drawable.destino_img_2);
						 break;
				    case 3:
				    	 intent.putExtra("resourceId", R.drawable.destino_img_3);
						 break;
				    case 4:
				    	 intent.putExtra("resourceId", R.drawable.destino_img_4);
						 break; 
				 }
				AdinivaImagenActivity.this.startActivity(intent);
				AdinivaImagenActivity.this.finish();
			}
			
		});
		
//		// Setting OK Button
//		alertDialog.setButton(null, "OK", new DialogInterface.OnClickListener() {
//	        public void onClick(DialogInterface dialog, int which) {
//	        // Write your code here to execute after dialog closed
//	        Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
//	        }
//		});
		
		
		// Showing Alert Message

		alertDialog.show();
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	
		return true;
	}
	
	/*
	public class YourAdapter extends BaseAdapter {

		   int rowsCount;

		   @Override
		   public View getView(int position, View convertView, ViewGroup parent) {

		       View itemView = convertView;
		       if (itemView == null) {
		           itemView = getLayoutInflater().inflate(R.layout.grid_item, parent, false);            
		           int height = parent.getHeight();
		           if (height > 0) {
		               LayoutParams layoutParams = (LayoutParams) itemView.getLayoutParams();
		               layoutParams.height = (int) (height / rowsCount);
		           } // for the 1st item parent.getHeight() is not calculated yet       
		       }
		   }

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

	}

	*/
}
