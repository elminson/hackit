package org.hackit.descubrerd.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.hackit.descubrerd.R;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.widget.WebDialog;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SummaryActivity extends Activity {
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private boolean pendingPublishReauthorization = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_summary);
		final Intent intent = getIntent();
		if (intent.hasExtra("title")){
			
			TextView titleTextView=(TextView)this.findViewById(R.id.titleTextView);
			titleTextView.setText(intent.getExtras().getString("title"));
		}
	    if (intent.hasExtra("description")){
	    	TextView titleTextView=(TextView)this.findViewById(R.id.descriptionTextView);
			titleTextView.setText(intent.getExtras().getString("description"));
		}
        if (intent.hasExtra("resourceId")){
        	
            ImageView imgView=(ImageView)this.findViewById(R.id.detailImageView);
            imgView.setImageDrawable(getResources().getDrawable(intent.getExtras().getInt("resourceId")));
        	
        	
			
		}
        
       
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.summary, menu);
		
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		   case R.id.ruta:
			   Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
			   Uri.parse("http://maps.google.com/maps?saddr=&daddr="+getIntent().getExtras().getString("lat")+","+getIntent().getExtras().getString("lon")));
			   startActivity(intent);
			   break;
		    default:
		    	 publishStory(getIntent().getExtras().getString("name"),getIntent().getExtras().getString("description"),getIntent().getExtras().getString("url"));
		    	 finish();
		}
		return true;
	
	}
	
	public void share(String nameApp,String title,String description, String imagePath) throws FileNotFoundException, NameNotFoundException {
		
		 String strFileName=getPackageManager().getPackageInfo(getPackageName(), 0).applicationInfo.dataDir+"/share.jpg";
        	Bitmap bitmap = ((BitmapDrawable)getResources().getDrawable(getIntent().getExtras().getInt("resourceId"))).getBitmap();
 		if(bitmap == null)
 			return ;
 		FileOutputStream file = null;

 			file = new FileOutputStream(strFileName);
 			bitmap.compress(CompressFormat.PNG, 100, file);
 	
 	
 			
 			Uri uriToImage = Uri.parse(strFileName);
		// Share
		Intent share = new Intent(Intent.ACTION_SEND);
		share.setType("image/*");

		share.putExtra(Intent.EXTRA_STREAM,uriToImage);
		share.putExtra(Intent.EXTRA_SUBJECT, title);
		share.putExtra(Intent.EXTRA_TEXT, description);

		startActivity(Intent.createChooser(share, "Share Image"));
	}
	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
	    for (String string : subset) {
	        if (!superset.contains(string)) {
	            return false;
	        }
	    }
	    return true;
	}
	private void publishStory(String title,String description,String url) {
	    Session session = Session.getActiveSession();

	    if (session != null){

	        // Check for publish permissions    
	        List<String> permissions = session.getPermissions();
	        if (!isSubsetOf(PERMISSIONS, permissions)) {
	            pendingPublishReauthorization = true;
	            Session.NewPermissionsRequest newPermissionsRequest = new Session
	                    .NewPermissionsRequest(this, PERMISSIONS);
	        session.requestNewPublishPermissions(newPermissionsRequest);
	            return;
	        }

	        Bundle postParams = new Bundle();
	        postParams.putString("name", "Acabo de descubrir un lugar");
	        postParams.putString("caption", title);
	        postParams.putString("description",description);
	        postParams.putString("link", "https://descubrerd.com");
	        postParams.putString("picture", url);

	        Request.Callback callback= new Request.Callback() {
	            public void onCompleted(Response response) {
	                JSONObject graphResponse = response
	                                           .getGraphObject()
	                                           .getInnerJSONObject();
	                String postId = null;
	                try {
	                    postId = graphResponse.getString("id");
	                } catch (JSONException e) {
	                    Log.i("SummaryActivity",
	                        "JSON error "+ e.getMessage());
	                }
	                FacebookRequestError error = response.getError();
	                if (error != null) {
	                    Toast.makeText(SummaryActivity.this
	                         .getApplicationContext(),
	                         error.getErrorMessage(),
	                         Toast.LENGTH_SHORT).show();
	                    } else {
	                        Log.d("SUMMARY", 
	                             postId);
	                        Toast.makeText(SummaryActivity.this
	   	                         .getApplicationContext(),
	   	                         "Posted on facebook!",
	   	                         Toast.LENGTH_SHORT).show();
	                }
	            }
	        };

	        Request request = new Request(session, "me/feed", postParams, 
	                              HttpMethod.POST, callback);

	        RequestAsyncTask task = new RequestAsyncTask(request);
	        task.execute();
	    }

	}
	

}
