package org.hackit.descubrerd;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.metaio.tools.io.AssetsManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MisDescubrimientosActivity extends Activity {
	 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mis_descubrimientos);
		
		final ListView listview = (ListView) findViewById(R.id.lista);

	    
	    final StableArrayAdapter adapter = new StableArrayAdapter(getApplicationContext(),
	    		R.layout.list_item, Arrays.asList(getResources().getStringArray(R.array.destinos)) );
	    
	    listview.setAdapter(adapter);

	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	      @Override
	      public void onItemClick(AdapterView<?> parent, final View view,
	          int position, long id) {
	    	  
	    	  Toast.makeText(getApplicationContext(), ""+position+" "+((TextView)view).getText(), Toast.LENGTH_LONG).show();
	    	  
	    	  Intent i = new Intent(getApplicationContext(), ARDescubreActivity.class );

	    	  try {
				AssetsManager.extractAllAssets(getApplicationContext(), true);
			  } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			  }
	    	  
	    	  final String arelConfigFilePath = AssetsManager.getAssetPath(getApplicationContext(), "AR/arelConfig.xml");
	    	  	    	 
	    	  
	    	  Log.d("RG_", "arelConfigFilePath: "+arelConfigFilePath);
	    	  //Log.d("RG_", "arelConfigFilePath: " );
	    	  
	    	  i.putExtra(getPackageName() + ".AREL_SCENE",
	    			  arelConfigFilePath);
	          i.putExtra("position", position);
	          startActivity(i);
	      }

	    });
		
	}
	
	private class StableArrayAdapter extends ArrayAdapter<String> {

	    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

	    public StableArrayAdapter(Context context, int textViewResourceId,
	        List<String> objects) {
	      super(context, textViewResourceId, objects);
	      for (int i = 0; i < objects.size(); ++i) {
	        mIdMap.put(objects.get(i), i);
	      }
	    }

	    @Override
	    public long getItemId(int position) {
	      String item = getItem(position);
	      return mIdMap.get(item);
	    }

	    @Override
	    public boolean hasStableIds() {
	      return true;
	    }

	  }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.mis_descubrimientos, menu);
		return true;
	}

}
