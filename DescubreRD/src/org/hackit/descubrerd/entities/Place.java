package org.hackit.descubrerd.entities;

import java.io.Serializable;

import com.google.android.gms.maps.model.Marker;

public class Place implements Serializable {
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private int id;
   private boolean isActive;
   private String name;
   private String city;
   private double latitude;
   private double longitude;
   private String description;
  private String imageUrl;

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public boolean isActive() {
	return isActive;
}
public void setActive(boolean isActive) {
	this.isActive = isActive;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public double getLatitude() {
	return latitude;
}
public void setLatitude(double latitude) {
	this.latitude = latitude;
}
public double getLongitude() {
	return longitude;
}
public void setLongitude(double longitude) {
	this.longitude = longitude;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getImageUrl() {
	return imageUrl;
}
public void setImageUrl(String imageUrl) {
	this.imageUrl = imageUrl;
}
   
}
