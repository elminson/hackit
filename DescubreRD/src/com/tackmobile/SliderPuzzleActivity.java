package com.tackmobile;

import java.util.Date;
import java.util.Random;


import org.hackit.descubrerd.R;
import org.hackit.descubrerd.entities.Place;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;

public class SliderPuzzleActivity extends Activity {
	
	private Drawable imagenImg;
	private Button meRindoBtn;
	
	private TextView scorePointsTv;
	
	private AlertDialog.Builder alertDialog;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.title_puzzle_activity);

        final Intent intent = getIntent();
		int randomImgInt =0;
		if (intent.hasExtra("Place")){
		    final Place myExtra= (Place)intent.
		        getSerializableExtra("Place");
		    randomImgInt =myExtra.getId();
		}else{
	     randomImgInt = new Random().nextInt(4);
		}
        initUi(randomImgInt);        
    }
    
    private void initUi(int id){
    	
		
//		int imgRes = getResources().getIdentifier("destino_img_"+randomImgInt, "id", this.getPackageName());

		Drawable destinoImg = null;
//		getResources().getDrawable(imgRes);
		
		
		switch (id) {
		case 0:
			destinoImg = getResources().getDrawable(R.drawable.destino_img_0);
			break;
		case 1:
			destinoImg = getResources().getDrawable(R.drawable.destino_img_1);
			break;
		case 2:
			destinoImg = getResources().getDrawable(R.drawable.destino_img_2);
			break;
		case 3:
			destinoImg = getResources().getDrawable(R.drawable.destino_img_3);
			break;
		case 4:
			destinoImg = getResources().getDrawable(R.drawable.destino_img_4);
			break;

		default:
			break;
		}
		

        LinearLayout ll = (LinearLayout) findViewById(R.id.mainContentLo);
        
        RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        GameboardView gameboarView = new GameboardView(this, destinoImg);
        
        ll.addView(gameboarView, relativeParams);
//        relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        
        meRindoBtn = (Button) findViewById(R.id.meRindoBtn);
        meRindoBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showDialog();
				
			}
		});
        
        scorePointsTv = (TextView) findViewById(R.id.scorePointsTv);
//        180 segundos o 3 minutos 
        new CountDownTimer(180000, 1000) {

            public void onTick(long millisUntilFinished) {
//            	
//            	Date tiempoRest = new Date(millisUntilFinished);
//            	
//            	int seconds = tiempoRest.getSeconds() % 60;
//            	int minutes = tiempoRest.getMinutes() / 60;
//            	String stringTime = String.format("%02d:%02d", minutes, seconds);
//            	
            	int secs = (int) (millisUntilFinished / 1000);
            	scorePointsTv.setText(""+secs);
            }

            public void onFinish() {
//                mTextField.setText("done!");
            	SliderPuzzleActivity.this.finish();
            	Toast.makeText(getApplicationContext(), "Tiempo superado", Toast.LENGTH_LONG).show();
            }
         }.start();
    }
    
	private boolean showDialog(){

		alertDialog = new AlertDialog.Builder(this);
		// Setting Dialog Title
		alertDialog.setTitle("DescubreRD");
		
		// Setting Dialog Message
		
//		String mensaje = "";
		
		alertDialog.setMessage("Confirme si est� seguro de que se rinde:");
		
		// Setting Icon to Dialog
//		alertDialog.setIcon(R.drawable.tick);
		
		alertDialog.setPositiveButton("S�, me rindo", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				SliderPuzzleActivity.this.finish();
			}
			
		});
		
		alertDialog.setNegativeButton("No, continuar", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// NADA, SOLO CONTINUAR JUGANDO
			}
			
		});
		
//		// Setting OK Button
//		alertDialog.setButton(null, "OK", new DialogInterface.OnClickListener() {
//	        public void onClick(DialogInterface dialog, int which) {
//	        // Write your code here to execute after dialog closed
//	        Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
//	        }
//		});
		
		
		// Showing Alert Message

		alertDialog.show();
		return true;
	}
}