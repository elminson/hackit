package org.hackit.rgdescubrerd;

import org.hackit.rgdescubrerd.R;
import com.tackmobile.SliderPuzzleActivity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity1 extends Activity {
	
	private Button lanzaAdivinaBtn;
	private Button lanzaTilePuzzleBtn;
	private Button lanzaDescubreBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main1);
		initUi();
	}

	private void initUi(){
		lanzaAdivinaBtn = (Button) findViewById(R.id.lanzaAdivinaBtn);
		lanzaAdivinaBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent lanzaAdivina = new Intent(getApplicationContext(), AdinivaImagenActivity.class);
				startActivity(lanzaAdivina);
			}
		});
		
		lanzaTilePuzzleBtn = (Button) findViewById(R.id.lanzaTilePuzzleBtn);
		lanzaTilePuzzleBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent lanzaPuzzleTileAct = new Intent(getApplicationContext(), SliderPuzzleActivity.class);
				startActivity(lanzaPuzzleTileAct);
				
			}
		});
		
		lanzaDescubreBtn = (Button) findViewById(R.id.lanzaDescubreBtn);
		lanzaDescubreBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent lanzaDescubreAct = new Intent(getApplicationContext(), MisDescubrimientosActivity.class);
				startActivity(lanzaDescubreAct);
				
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_activity1, menu);
		return true;
	}

}
