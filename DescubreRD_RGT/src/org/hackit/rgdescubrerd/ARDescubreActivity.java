package org.hackit.rgdescubrerd;

import org.hackit.rgdescubrerd.R;
import com.metaio.sdk.MetaioDebug;
import com.metaio.sdk.jni.IGeometry;
import com.metaio.sdk.jni.IMetaioSDKCallback;
import com.metaio.sdk.jni.Vector3d;
import com.metaio.tools.io.AssetsManager;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;

public class ARDescubreActivity extends ARELViewActivity {

	private IGeometry mModel;

	private int position = 0;
	
	@Override
	protected int getGUILayout() 
	{
		// Attaching layout to the activity
		
		try {
			position = getIntent().getExtras().getInt("position");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return R.layout.activity_ardescubre; 
	}


	public void onButtonClick(View v)
	{
		finish();
	}
	
	
	@Override
	protected void loadContents() 
	{
		try
		{
			// Getting a file path for tracking configuration XML file
			String trackingConfigFile = AssetsManager.getAssetPath(getApplicationContext(), "AR/Assets/TrackingData_MarkerlessFast.xml");
						
			// Assigning tracking configuration
			boolean result = metaioSDK.setTrackingConfiguration(trackingConfigFile); 
			
			MetaioDebug.log("Tracking data loaded: " + result); 
	        
			// Getting a file path for a 3D geometry
//			String metaioManModel = AssetsManager.getAssetPath(getApplicationContext(), "TutorialHelloWorld/Assets/metaioman.md2");
			String metaioManModel = AssetsManager.getAssetPath(getApplicationContext(), get3DModelPathString(position));
			if (metaioManModel != null) 
			{
				// Loading 3D geometry
				mModel = metaioSDK.createGeometry(metaioManModel);
				if (mModel != null) 
				{
					// Set geometry properties
					mModel.setScale(new Vector3d(4.0f, 4.0f, 4.0f));
					
				}
				else
					MetaioDebug.log(Log.ERROR, "Error loading geometry: "+metaioManModel);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static String get3DModelPathString(int position){
		String ruta = "monumento_heroes.obj";
		
		switch(position){
		case 0:
			ruta = "faro_colon.obj";
			break;
		case 1:
			ruta = "alcazar_colon.obj";
			break;
		case 2:
			ruta = "monumento_heroes.obj";
			break;
		case 3:
			ruta = "parque_puerto_plata.obj";
			break;
		case 4:
			ruta = "puerto_san_felipe.obj";
			break;
		}
		
		return "AR/Assets/"+ruta;
	}


	@Override
	protected void onGeometryTouched(IGeometry geometry)
	{
		// Not used in this tutorial
	}


	@Override
	protected IMetaioSDKCallback getMetaioSDKCallbackHandler()
	{
		// No callbacks needed in this tutorial
		return null;
	}

}
