// Copyright 2007-2014 metaio GmbH. All rights reserved.
package org.hackit.rgdescubrerd;


import android.view.View;

import org.hackit.rgdescubrerd.R;
import com.metaio.sdk.ARELActivity;


public class ARELViewActivity extends ARELActivity 
{
	@Override
	protected int getGUILayout() 
	{
		return R.layout.arel;
	}
	
	public void onButtonClick(View v)
	{
		finish();
	}
 
}
